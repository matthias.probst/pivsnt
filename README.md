# Standard Name Table for Particle Image Velocimetry (PIV) data

![version](https://img.shields.io/badge/version-experimental-red)
![proj](https://img.shields.io/badge/convention-PIVConvention-infrmational)

This repository manages the standard name table used to describe the physical quantities of PIV data. A "standard_name"
and its correct unit are to be attached to a variable in the PIV data file. It is recommended to use HDF5 as each
dataset can be annotated with attributes.

The table lists all specified standard names with their SI unit and a human-readable description. The table 
may not be complete and is intended to be extended and improved continuously. A version string clearly indicates the 
version of the table.

> **_NOTE:_**  The standard name table is still in an experimental state.

## What's a standard name?

A standard name is constructed by a combination of pre- and postfixes and a standard name as outlined below. It is
further associated with a physical unit and a human-readable description. The standard name table with unit and
description is listed in a Standard Name Table, which this repository contains.

Standard names and thus the content of the table undergoes continuous chang and improvements. Via the git-issue system
new standard names or changes can be discussed. A version string clearly indicates the version of the table.

> **_NOTE:_** PIV can be applied to various fluids such as air or water for instance. The standard name table here does 
> not distinguish between different fluids. Other meta information such as `piv_medium` needs to be set to indicate the 
> medium in which the PIV measurement was performed.

### Construction rules

A "standard name" describes a physical quantity. It must comply with the following rules:

- They are lower case
- They are separated by underscores
- They are not allowed to start with a number
- They are not allowed to contain spaces
- They are not allowed to contain special characters (except for underscores)

The standard name may contain pre- or postfixes separated by underscores. Different pre- or postfixes classes are
outlined here:

```
[surface][vector component]
<standard_name>
[at suface][in medium]
[before/after/across/of component][due to process][assuming condition
```

Additionally, it is possible to construct derivatives of the standard name by adding a prefixes like

- difference_of_
- derivative_of_

### Examples

Let us assume we want to describe the velocity of a fluid. The standard name would be "velocity". It could be extended
by the prefix "x_" to indicate the x-component of the velocity vector. The postfix "_at_surface" could be used to
indicate that the velocity is measured at a surface.

## Disclaimer

The standard name and table is based on the [CF-Convention](https://cfconventions.org/standard-names.html). The
construction rules are also based on it but are extended to fit the needs of PIV data.


